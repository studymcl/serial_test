﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //シリアルポートのクローズ処理
            if (serialPort1.IsOpen == false)
            {
                serialPort1.Open();
            }
            //通信設定
            serialPort1.PortName = "COM1";
            serialPort1.BaudRate = 9600;
            serialPort1.DataBits = 8;
            serialPort1.Parity = System.IO.Ports.Parity.None;
            serialPort1.StopBits = System.IO.Ports.StopBits.One;
            serialPort1.Encoding = System.Text.Encoding.GetEncoding(932);
        }
       
        //デリゲート　
        private delegate void RecieveDel(string reciveData);
        
        //デリゲートから呼び出されるメソッドの宣言
        private void SetRecieve(string dataString)
        {
            textBox1.Text = dataString;
        }
        
        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            string recieveData;
            
            RecieveDel recive = new RecieveDel(SetRecieve);
            try
            {
                recieveData = serialPort1.ReadLine();
            }
            catch (Exception ex)
            {
                recieveData = ex.Message;
            }
            Invoke(recive,recieveData+"\r\n");
        }
    }
}
